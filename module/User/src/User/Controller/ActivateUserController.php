<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ActivateUserController extends AbstractActionController
{
    public function indexAction()
    {
        if ($this->zfcUserAuthentication()->hasIdentity()) {
            $this->redirect('userhome');
        } else {
          if($this->event->getRouteMatch()->getParam('code')) {
            $activateCode = $this->event->getRouteMatch()->getParam('code');
            $updateUser = $this->getServiceLocator()->get('User\Mapper\UserMapper')->activateUser($activateCode);
            if($updateUser === 'success') {
              $view = new ViewModel();
              $view->setTemplate('success');
              return $view;
            } else {
              $view = new ViewModel();
              $view->setTemplate('failure');
              return $view;
            }
          }
        }
    }
}
