<?php

namespace User\Mapper;

use Zend\Db\TableGateway\TableGateway;

class UserMapper
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function activateUser($code)
    {
        $rowSet = $this->tableGateway->select(array('code' => $code));
        $row = $rowSet->current();
        if(!$row) {
            return 'failure';
        } else {
            $data = array(
                'username' => $row->getUsername(),
                'email' => $row->getEmail(),
                'display_name' => $row->display_name,
                'code' => $row->getCode(),
                'state' => '1',
            );
            $this->tableGateway->update($data, array('user_id' => $row->user_id));
            return 'success';
        }
    }
}