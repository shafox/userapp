<?php

namespace User\Model;

use ZfcUser\Entity\User as ZfcUser;

class User extends ZfcUser
{
    protected $code;

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    public function exchangeArray($data)
     {
         $this->user_id     = (!empty($data['user_id'])) ? $data['user_id'] : null;
         $this->username = (!empty($data['username'])) ? $data['username'] : null;
         $this->email  = (!empty($data['email'])) ? $data['email'] : null;
         $this->display_name  = (!empty($data['display_name'])) ? $data['display_name'] : null;
         $this->password  = (!empty($data['country'])) ? $data['country'] : null;
         $this->state  = (!empty($data['state'])) ? $data['state'] : null;
         $this->code  = (!empty($data['code'])) ? $data['code'] : null;
     }
}