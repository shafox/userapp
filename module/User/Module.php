<?php
namespace User;

use Zend\Mvc\MvcEvent;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;


use User\Model\User;
use User\Mapper\UserMapper;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;


class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    public function onBootstrap( MVCEvent $e )
    {
        $eventManager = $e->getApplication()->getEventManager();
        $em           = $eventManager->getSharedManager();

        $zfcServiceEvents = $e->getApplication()->getServiceManager()->get('zfcuser_user_service')->getEventManager();

        $zfcServiceEvents->attach('register', function($e) {
            $form = $e->getParam('form');
            $user = $e->getParam('user');

            $user->setState( 0 );
            $random_hash = md5(uniqid(rand(), true));
            $user->setCode( $random_hash );
        });

        $zfcServiceEvents->attach('register.post', function($e) {
            $user = $e->getParam('user');
            $message = new Message();
            $message->setSubject('Email Verification');
            $message->setFrom('behera.pranaya@gmail.com', 'Pranaya Behera');
            $message->setBody('Your verification code is: http://userapp.dev/email/confirm/'.$user->getCode().' ');
            $message->addTo($user->getEmail(), $user->getDisplayName());
            $transport = new SmtpTransport();
            $options   = new SmtpOptions(array(
                'name'              => 'mandrillapp.com',
                'host'              => 'smtp.mandrillapp.com',
                'port'              => 587,
                'connection_class'  => 'plain',
                'connection_config' => array(
                    'username' => 'pranaya.behera@outlook.com',
                    'password' => 'G3EsGU10lDPYzKQ8bbmwxA',
                    'ssl'      => 'tls'
                ),
            ));
            $transport->setOptions($options);
            $transport->send($message);
        });
    }


    public function getServiceConfig()
     {
         return array(
             'factories' => array(
                 'User\Mapper\UserMapper' =>  function($sm) {
                     $tableGateway = $sm->get('UserTableGateway');
                     $table = new UserMapper($tableGateway);
                     return $table;
                 },
                 'UserTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new User());
                     return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
                 },
             ),
         );
     }
}
