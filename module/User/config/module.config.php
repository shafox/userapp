<?php
return array(
    'service_manager' => array(
        'factories' => array(
        ),
        'aliases' => array(
            'Soflomo\Mail\Transport' => 'SlmMail\Mail\Transport\MandrillTransport',
        ),
    ),
    'router' => array(
        'routes' => array(
            'email-confirm' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/email/confirm/:code',
                    'defaults' => array(
                        'controller' => 'User\Controller\ActivateUserController',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'User\Controller\ActivateUserController' => 'User\Controller\ActivateUserController'
        ),
    ),
    'service_manager' => array(

        'factories' => array(
             'Zend\Db\Adapter\Adapter'
                     => 'Zend\Db\Adapter\AdapterServiceFactory',
         ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        /*'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',*/
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/user/layout/layout.phtml',
            /*'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',*/
            'failure' => __DIR__ . '/../view/user/failure.phtml',
            'success' => __DIR__ . '/../view/user/success.phtml'
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);